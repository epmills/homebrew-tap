class Blurz < Formula
    desc "Blurring text through obfuscation"
    homepage "https://gitlab.com/epmills/zig-blur"
    version "0.1.0"
    bottle :unneeded
  
    if OS.mac?
      url "https://gitlab.com/epmills/zig-blur/uploads/e2764db075933cf8cf629e2d9f1b3a12/blurz_x86_64-macosx-gnu.tar.gz"
      sha256 "712c94712a679975e37da030b259850732ec0a77f1acd3ba9816f2ae412c5e52"
    elsif OS.linux?
      if Hardware::CPU.intel?
        url "https://gitlab.com/epmills/zig-blur/uploads/61d5540a2b0fc43cd30d0e23833e0a62/blurz_x86_64-linux-musl.tar.gz"
        sha256 "508ab2fbada3804d3e3f0120a5ebd90a1967e9658ce25263d3b9b7900b85c32a"
      end
    end
  
    def install
      bin.install "blurz"
    end

    test do
      assert_match "blurz 0.1.0", shell_output("#{bin}/blurz -V", 2)
    end

  end