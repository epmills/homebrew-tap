class Dialz < Formula
    desc "Translate a string into its equivalent digits on a phone"
    homepage "https://gitlab.com/epmills/zig-dial"
    version "0.1.2"
    bottle :unneeded
  
    if OS.mac?
      url "https://gitlab.com/epmills/zig-dial/uploads/779e42052d6ec4e9622c69f8731317ea/dialz_x86_64-macosx-gnu.tar.gz"
      sha256 "b1002a4bec70e6ad67a8e32333c7f4a9838ab5b5b2265708b8ded61f4cfe6a90"
    elsif OS.linux?
      if Hardware::CPU.intel?
        url "https://gitlab.com/epmills/zig-dial/uploads/a1246e1ac1d6ef0ae4bebd2d6c63445d/dialz_x86_64-linux-musl.tar.gz"
        sha256 "7ffa4531f3734785bf45f63516245788afbd25ef7e9287fdb61d0254d7365eb7"
      end
    end
  
    def install
      bin.install "dialz"
    end

    test do
      assert_match "dialz 0.1.2", shell_output("#{bin}/dialz -V", 2)
    end

  end