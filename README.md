# epmills Tap

Homebrew formulae for my tools and utilities.


## Available Formulae

| Formula   | Description                                              |
|-----------|----------------------------------------------------------|
| **dialz** | Translate a string into its equivalent digits on a phone |
| **blurz** | Blurring text through obfuscation                        |


## Installation

First, add this tap to Homebrew:
```shell
$ brew tap epmills/tap https://gitlab.com/epmills/homebrew-tap.git
```
This only needs to be done once.

Then, to install a formula:
```shell
$ brew install <formula>
```


## Homebrew Help
For help with Homebrew, execute `brew help` or `man brew` or check [Homebrew's documentation](https://docs.brew.sh).


## Credits
[Ameer A.'s Gitlab repository](https://gitlab.com/ojizero/homebrew-tap) for providing a Homebrew tap template.